﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Inscripto
    {
        public int NroAutoIncremental { get; set; }
        public int DNIParticipante { get; set; }
        public int NumeroTelefono { get; set; }

        public int AutoIncrementar()
        {
            return NroAutoIncremental += 1;
        }

    }
}
