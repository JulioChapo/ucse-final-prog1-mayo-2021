﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Sorteo
    {
        public int CodigoAutoincremental { get; set; }
        public int CodigoPrograma { get; set; }
        public int DNIGanador { get; set; }
        public string DescripcionPremio { get; set; }
        public List<Inscripto> ListaInscriptos { get; set; }
        public bool Ganado { get; set; }

        public int AutoIncrementar()
        {
            return CodigoAutoincremental += 1;
        }

        public string Sortear()
        {
            
            Random Random = new Random();
            return Random.Next(1,(ListaInscriptos.Count));
        }
    }
}
