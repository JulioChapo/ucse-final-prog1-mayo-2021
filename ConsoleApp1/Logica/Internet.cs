﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Internet: Plataforma
    {
        public streaming PlataformaStreaming { get; set; }
        public enum streaming
        {
            Instagram,
            Facebook,
            Youtube
        }
        public DateTime DiaStreaming { get; set; }
        public string NombreStreaming { get; set; }
        public string ContentManager { get; set; }

        public override string SiMismo()
        {
            return $"No te pierdas en {PlataformaStreaming} el streaming {NombrePrograma} llevado adelante por {ContentManager}";
        }
    }
}
