﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Radio: Plataforma
    {
        public string Conductor { get; set; }
        public int Frecuencia { get; set; }

        public override string SiMismo()
        {
            return $"Tu Radio Favorita Conducida por: {Conductor}, Dias: {DiaSemana} siempre en la misma Frecuencia: {Frecuencia}";
        }
    }
}
