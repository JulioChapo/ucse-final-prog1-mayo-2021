﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
     public abstract class Plataforma
    {
        public int CodigoPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public DateTime FechaEmision { get; set; }
        public string Categoria { get; set; }
        public Dia DiaSemana { get; set; }
        public enum Dia
        {
            Lunes,
            Martes,
            Miercoles,
            Jueves,
            Viernes
        }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFinal { get; set; }

        public void GenerarCodigoAutoincremental()
        {
            CodigoPrograma += 1;
        }

        public abstract string SiMismo();

        public abstract List<string> ObtenerDescripciones(Dia diaSemana, string plataforma );

    }
}
