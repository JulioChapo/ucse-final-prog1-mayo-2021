﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Television: Plataforma
    {
        public string Conductor1 { get; set; }
        public string Conductor2 { get; set; }
        public int Canal { get; set; }


        public override string SiMismo()
        {
            return $"{NombrePrograma} Conductores: {Conductor1}, {Conductor2}, Dias: {DiaSemana} desde las {HoraInicio} hasta las {HoraFinal} por el canal: {Canal}";
        }

        public override List<string> ObtenerDescripciones(Dia diaSemana, string plataforma)
        {
            
        }
    }
}
